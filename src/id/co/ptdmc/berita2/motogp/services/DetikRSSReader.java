package id.co.ptdmc.berita2.motogp.services;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * An RSS Reader class that reads RSS from detik.com.
 *
 * @author Rochmat Santoso
 * */
public final class DetikRSSReader extends BaseRSSReader {

    /**
     * RSS URL.
     * */
    private static final String URL = "http://detik.feedsportal.com/c/33613/f/656102/index.rss";

    /**
     * Detik.com's news source.
     * */
    private static final String NEWS_SOURCE = "detik.com";

    /**
     * Tag of publish date.
     * */
    private static final String TAG_PUBLISH_DATE = "pubDate";

    /**
     * Tag of thumbnail image.
     * */
    private static final String TAG_THUMBNAIL = "enclosure";

    /**
     * Date format to format published date.
     * */
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
            "EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);

    @Override
    protected String getURL() {
        return URL;
    }

    @Override
    protected String getPublishDateTag() {
        return TAG_PUBLISH_DATE;
    }

    @Override
    protected SimpleDateFormat getPublishDateFormatter() {
        return DATE_FORMAT;
    }

    @Override
    protected String getThumbnailTag() {
        return TAG_THUMBNAIL;
    }

    @Override
    protected String getNewsSource() {
        return NEWS_SOURCE;
    }

}
