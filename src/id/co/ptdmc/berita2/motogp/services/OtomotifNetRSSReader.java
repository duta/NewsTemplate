package id.co.ptdmc.berita2.motogp.services;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * An RSS Reader class that reads RSS from otomotifnet.com.
 *
 * @author Rochmat Santoso
 * */
public final class OtomotifNetRSSReader extends BaseRSSReader {

    /**
     * RSS URL.
     * */
    private static final String URL = "http://otomotifnet.com/index.php/feed/43/MotoGP/";

    /**
     * Otomotifnet.com's news source.
     * */
    private static final String NEWS_SOURCE = "otomotifnet.com";

    /**
     * Tag of publish date.
     * */
    private static final String TAG_PUBLISH_DATE = "pubDate";

    /**
     * Date format to format published date.
     * */
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
            "EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);

    @Override
    protected String getURL() {
        return URL;
    }

    @Override
    protected String getPublishDateTag() {
        return TAG_PUBLISH_DATE;
    }

    @Override
    protected String getThumbnailTag() {
        return null;
    }

    @Override
    protected SimpleDateFormat getPublishDateFormatter() {
        return DATE_FORMAT;
    }

    @Override
    protected String getNewsSource() {
        return NEWS_SOURCE;
    }

}
