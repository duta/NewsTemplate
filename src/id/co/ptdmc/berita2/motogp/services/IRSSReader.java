package id.co.ptdmc.berita2.motogp.services;

import id.co.ptdmc.berita2.motogp.beans.MotoGP;

import java.util.List;

/**
 * An interface of all RSS Reader classes.
 *
 * @author Rochmat Santoso
 * */
public interface IRSSReader {

    /**
     * Read data from RSS.
     *
     * @return a list {@link MotoGP} data
     * */
    List<MotoGP> read();

}
