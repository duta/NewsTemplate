package id.co.ptdmc.berita2.motogp.services;

import id.co.ptdmc.berita2.motogp.beans.MotoGP;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

/**
 * Base class of all RSS Reader classes.
 * */
public abstract class BaseRSSReader implements IRSSReader {

    /**
     * Tag of rss.
     * */
    protected static final String TAG_RSS = "rss";

    /**
     * Tag of channel.
     * */
    protected static final String TAG_CHANNEL = "channel";

    /**
     * Tag of item.
     * */
    protected static final String TAG_ITEM = "item";

    /**
     * Tag of item's title.
     * */
    protected static final String TAG_TITLE = "title";

    /**
     * Tag of item's link.
     * */
    protected static final String TAG_LINK = "link";

    /**
     * Tag of item's description.
     * */
    protected static final String TAG_DESCRIPTION = "description";

    /**
     * Get RSS URL.
     *
     * @return the URL
     * */
    protected abstract String getURL();

    /**
     * Get tag of publish date.
     *
     * @return the tag name
     * */
    protected abstract String getPublishDateTag();

    /**
     * Get tag of thumbnail image.
     *
     * @return the tag name
     * */
    protected abstract String getThumbnailTag();

    /**
     * Get date format to format published date.
     *
     * @return the date formatter
     * */
    protected abstract SimpleDateFormat getPublishDateFormatter();

    /**
     * Get news source.
     *
     * @return the news source
     * */
    protected abstract String getNewsSource();

    @Override
    public final List<MotoGP> read() {
        try {
            List<MotoGP> result = new ArrayList<MotoGP>();

            InputStream stream = getStream();
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(stream, null);

            int event = parser.next();
            if (event == XmlPullParser.START_TAG && TAG_RSS.equalsIgnoreCase(parser.getName())) {
                while (!(event == XmlPullParser.START_TAG && TAG_CHANNEL.equalsIgnoreCase(parser.getName()))) {
                    event = parser.next();
                }
                while (!(event == XmlPullParser.END_TAG && TAG_CHANNEL.equalsIgnoreCase(parser.getName()))) {
                    if (event == XmlPullParser.START_TAG && TAG_ITEM.equalsIgnoreCase(parser.getName())) {
                        addItem(parser, result);
                    }
                    event = parser.next();
                }
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Get RSS Reader input stream from the url.
     *
     * @return the input stream
     * @throws IOException thrown if an error occurs during HTTP request
     * */
    private InputStream getStream() throws IOException {
        URL url = new URL(getURL());

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        return conn.getInputStream();
    }

    /**
     * Add item from RSS to a list.
     *
     * @param parser the xml parser
     * @param result the result list
     * @throws IOException thrown if an error occurs during xml parsing
     * @throws XmlPullParserException thrown if an error occurs during xml parsing
     * @throws ParseException thrown if an error occurs during date parsing
     * */
    protected void addItem(
            final XmlPullParser parser,
            final List<MotoGP> result)
            throws XmlPullParserException, IOException, ParseException {
        MotoGP obj = new MotoGP();
        obj.setSource(getNewsSource());
        int event = parser.next();
        while (!(event == XmlPullParser.END_TAG && TAG_ITEM.equalsIgnoreCase(parser.getName()))) {
            if (event == XmlPullParser.START_TAG && TAG_TITLE.equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    obj.setTitle(text);
                }
            } else if (event == XmlPullParser.START_TAG && TAG_LINK.equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    obj.setLink(text);
                }
            } else if (event == XmlPullParser.START_TAG && TAG_DESCRIPTION.equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    obj.setDescription(text);
                }
            } else if (event == XmlPullParser.START_TAG && getPublishDateTag().equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    Date date = getPublishDateFormatter().parse(text);
                    obj.setPublishDate(date);
                }
            } else if (getThumbnailTag() != null && event == XmlPullParser.START_TAG && getThumbnailTag().equalsIgnoreCase(parser.getName())) {
                String imageURL = parser.getAttributeValue(null, "url");
                obj.setThumbnail(imageURL);
            }
            event = parser.next();
        }
        if (obj.getTitle() != null && obj.getLink() != null && obj.getDescription() != null) {
            obj.setRead(false);
            result.add(obj);
        }
    }

}
