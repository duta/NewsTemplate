package id.co.ptdmc.berita2.motogp.services;

import id.co.ptdmc.berita2.motogp.beans.MotoGP;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/**
 * An RSS Reader class that reads RSS from metrotvnews.com.
 *
 * @author Rochmat Santoso
 * */
public final class MetroTVNewsRSSReader extends BaseRSSReader {

    /**
     * RSS URL.
     * */
    private static final String URL = "http://olahraga.metrotvnews.com/feed/motogp";

    /**
     * Metrotvnews.com's news source.
     * */
    private static final String NEWS_SOURCE = "metrotvnews.com";

    /**
     * Tag of publish date.
     * */
    private static final String TAG_PUBLISH_DATE = "pubDate";

    /**
     * Date format to format published date.
     * */
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
            "EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);

    /**
     * Tag of category.
     * */
    private static final String TAG_CATEGORY = "category";

    /**
     * Category value of motogp.
     * */
    private static final String CATEGORY_MOTOGP = "Moto GP";

    @Override
    protected String getURL() {
        return URL;
    }

    @Override
    protected String getPublishDateTag() {
        return TAG_PUBLISH_DATE;
    }

    @Override
    protected String getThumbnailTag() {
        return null;
    }

    @Override
    protected SimpleDateFormat getPublishDateFormatter() {
        return DATE_FORMAT;
    }

    @Override
    protected void addItem(
            final XmlPullParser parser,
            final List<MotoGP> result)
            throws XmlPullParserException, IOException, ParseException {
        boolean isMotoGP = false;
        MotoGP obj = new MotoGP();
        obj.setSource(getNewsSource());
        int event = parser.next();
        while (!(event == XmlPullParser.END_TAG && TAG_ITEM.equalsIgnoreCase(parser.getName()))) {
            if (event == XmlPullParser.START_TAG && TAG_TITLE.equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    obj.setTitle(text);
                }
            } else if (event == XmlPullParser.START_TAG && TAG_LINK.equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    obj.setLink(text);
                }
            } else if (event == XmlPullParser.START_TAG && TAG_DESCRIPTION.equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    obj.setDescription(text);
                }
            } else if (event == XmlPullParser.START_TAG && getPublishDateTag().equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    Date date = getPublishDateFormatter().parse(text);
                    obj.setPublishDate(date);
                }
            } else if (getThumbnailTag() != null && event == XmlPullParser.START_TAG && getThumbnailTag().equalsIgnoreCase(parser.getName())) {
                String imageURL = parser.getAttributeValue(null, "url");
                obj.setThumbnail(imageURL);
            } else if (event == XmlPullParser.START_TAG && TAG_CATEGORY.equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    if (text.equalsIgnoreCase(CATEGORY_MOTOGP)) {
                        isMotoGP = true;
                    }
                }
            }
            event = parser.next();
        }
        if (obj.getTitle() != null && obj.getLink() != null && obj.getDescription() != null && isMotoGP) {
            obj.setRead(false);
            result.add(obj);
        }
    }

    @Override
    protected String getNewsSource() {
        return NEWS_SOURCE;
    }

}
