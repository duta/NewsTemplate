package id.co.ptdmc.berita2.motogp.services;

import id.co.ptdmc.berita2.motogp.beans.MotoGP;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/**
 * An RSS Reader class that reads RSS from sindonews.
 *
 * @author Rochmat Santoso
 * */
public final class SindoNewsRSSReader extends BaseRSSReader {

    /**
     * RSS URL.
     * */
    private static final String URL = "http://sports.sindonews.com/rss";

    /**
     * Sindonews.com's news source.
     * */
    private static final String NEWS_SOURCE = "sindonews.com";

    /**
     * Tag of publish date.
     * */
    private static final String TAG_PUBLISH_DATE = "pubDate";

    /**
     * Tag of thumbnail image.
     * */
    private static final String TAG_THUMBNAIL = "media:content";

    /**
     * Tag of content.
     * */
    private static final String TAG_CONTENT = "content:encoded";

    /**
     * Date format to format published date.
     * */
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
            "EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);

    /**
     * A string to differentiate between motogp entry and other entries.
     * */
    private static final String STR_MOTOGP = "motogp";

    @Override
    protected String getURL() {
        return URL;
    }

    @Override
    protected String getPublishDateTag() {
        return TAG_PUBLISH_DATE;
    }

    @Override
    protected String getThumbnailTag() {
        return TAG_THUMBNAIL;
    }

    @Override
    protected SimpleDateFormat getPublishDateFormatter() {
        return DATE_FORMAT;
    }

    /**
     * Add item from RSS to a list.
     *
     * @param parser the xml parser
     * @param result the result list
     * @throws IOException thrown if an error occurs during xml parsing
     * @throws XmlPullParserException thrown if an error occurs during xml parsing
     * @throws ParseException thrown if an error occurs during date parsing
     * */
    protected void addItem(
            final XmlPullParser parser,
            final List<MotoGP> result)
            throws XmlPullParserException, IOException, ParseException {
        boolean isMotoGP = false;
        MotoGP obj = new MotoGP();
        obj.setSource(getNewsSource());
        int event = parser.next();
        while (!(event == XmlPullParser.END_TAG && TAG_ITEM.equalsIgnoreCase(parser.getName()))) {
            if (event == XmlPullParser.START_TAG && TAG_TITLE.equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    obj.setTitle(text);
                }
            } else if (event == XmlPullParser.START_TAG && TAG_LINK.equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    obj.setLink(text);
                }
            } else if (event == XmlPullParser.START_TAG && TAG_DESCRIPTION.equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    obj.setDescription(text);
                }
            } else if (event == XmlPullParser.START_TAG && getPublishDateTag().equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    Date date = getPublishDateFormatter().parse(text);
                    obj.setPublishDate(date);
                }
            } else if (getThumbnailTag() != null && event == XmlPullParser.START_TAG && getThumbnailTag().equalsIgnoreCase(parser.getName())) {
                String imageURL = parser.getAttributeValue(null, "url");
                obj.setThumbnail(imageURL);
            } else if (event == XmlPullParser.START_TAG && TAG_CONTENT.equalsIgnoreCase(parser.getName())) {
                event = parser.next();
                if (event == XmlPullParser.TEXT) {
                    String text = parser.getText();
                    if (text.toLowerCase(Locale.getDefault()).contains(STR_MOTOGP)) {
                        isMotoGP = true;
                    }
                }
            }
            event = parser.next();
        }
        if (obj.getTitle() != null && obj.getLink() != null && obj.getDescription() != null && isMotoGP) {
            obj.setRead(false);
            result.add(obj);
        }
    }

    @Override
    protected String getNewsSource() {
        return NEWS_SOURCE;
    }

}
