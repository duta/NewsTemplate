package id.co.ptdmc.berita2.motogp.services;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

/**
 * An RSS Reader class that reads RSS from autosport.
 *
 * @author Rochmat Santoso
 * */
public final class AutoSportRSSReader extends BaseRSSReader {

    /**
     * RSS URL.
     * */
    private static final String URL = "http://www.autosport.com/rss/motogpnews.xml";

    /**
     * Autosport's news source.
     * */
    private static final String NEWS_SOURCE = "autosport.com";

    /**
     * Tag of publish date.
     * */
    private static final String TAG_PUBLISH_DATE = "pubDate";

    /**
     * Date format to format published date.
     * */
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
            "EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);

    @Override
    protected String getURL() {
        return URL;
    }

    @Override
    protected String getPublishDateTag() {
        return TAG_PUBLISH_DATE;
    }

    @Override
    protected SimpleDateFormat getPublishDateFormatter() {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT"));
        return DATE_FORMAT;
    }

    @Override
    protected String getThumbnailTag() {
        return null;
    }

    @Override
    protected String getNewsSource() {
        return NEWS_SOURCE;
    }

}
