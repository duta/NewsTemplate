package id.co.ptdmc.berita2.motogp.services;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * An RSS Reader class that reads RSS from highrevs.net.
 *
 * @author Rochmat Santoso
 * */
public final class HighRevsRSSReader extends BaseRSSReader {

    /**
     * RSS URL.
     * */
    private static final String URL = "http://www.highrevs.net/motogp-rss/news_feed.xml";

    /**
     * Highrevs.net's news source.
     * */
    private static final String NEWS_SOURCE = "highrevs.net";

    /**
     * Tag of publish date.
     * */
    private static final String TAG_PUBLISH_DATE = "dc:date";

    /**
     * Date format to format published date.
     * */
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
            "yyyy-MM-dd", Locale.US);

    @Override
    protected String getURL() {
        return URL;
    }

    @Override
    protected String getPublishDateTag() {
        return TAG_PUBLISH_DATE;
    }

    @Override
    protected SimpleDateFormat getPublishDateFormatter() {
        return DATE_FORMAT;
    }

    @Override
    protected String getThumbnailTag() {
        return null;
    }

    @Override
    protected String getNewsSource() {
        return NEWS_SOURCE;
    }

}
