/**
 * A package that contains all bean classes.
 *
 * @author Rochmat Santoso
 */
package id.co.ptdmc.berita2.motogp.beans;
