package id.co.ptdmc.berita2.motogp.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * A class that holds motogp information.
 *
 * @author Rochmat Santoso
 * */
public final class MotoGP implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7917053748589880119L;

    /**
     * Title of the news.
     * */
    private String title;

    /**
     * Brief description of the news.
     * */
    private String description;

    /**
     * News source.
     * */
    private String source;

    /**
     * A link to the actual news.
     * */
    private String link;

    /**
     * Image thumbnail.
     * */
    private String thumbnail;

    /**
     * News' published date.
     * */
    private Date publishDate;

    /**
     * A flag that indicates the news has been read.
     * */
    private boolean read;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param paramTitle the title to set
     */
    public void setTitle(final String paramTitle) {
        this.title = paramTitle;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param paramDescription the description to set
     */
    public void setDescription(final String paramDescription) {
        this.description = paramDescription;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param paramSource the source to set
     */
    public void setSource(final String paramSource) {
        this.source = paramSource;
    }

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param paramLink the link to set
     */
    public void setLink(final String paramLink) {
        this.link = paramLink;
    }

    /**
     * @return the thumbnail
     */
    public String getThumbnail() {
        return thumbnail;
    }

    /**
     * @param paramThumbnail the thumbnail to set
     */
    public void setThumbnail(final String paramThumbnail) {
        this.thumbnail = paramThumbnail;
    }

    /**
     * @return the publishDate
     */
    public Date getPublishDate() {
        return publishDate;
    }

    /**
     * @param paramPublishDate the publishDate to set
     */
    public void setPublishDate(final Date paramPublishDate) {
        this.publishDate = paramPublishDate;
    }

    /**
     * @return the read
     */
    public boolean isRead() {
        return read;
    }

    /**
     * @param paramRead the read to set
     */
    public void setRead(final boolean paramRead) {
        this.read = paramRead;
    }

    @Override
    public String toString() {
        return "MotoGP [title=" + title + ", description=" + description
                + ", link=" + link + ", thumbnail=" + thumbnail
                + ", publishDate=" + publishDate + ", read=" + read + "]";
    }

}
