package id.co.ptdmc.berita2.motogp;

import id.co.ptdmc.berita2.motogp.beans.MotoGP;
import id.co.ptdmc.berita2.motogp.utils.BitmapCacheUtil;
import id.co.ptdmc.berita2.motogp.utils.CommonUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * An adapter class for displaying a list of {@link MotoGP} data.
 *
 * @author Rochmat Santoso
 * */
public final class MainAdapter extends ArrayAdapter<MotoGP> {

    /**
     * Application context.
     * */
    private Context context;

    /**
     * List of {@link MotoGP} object.
     * */
    private List<MotoGP> data;

    /**
     * @param ctx application context
     * @param objects the data to display
     * */
    public MainAdapter(final Context ctx, final List<MotoGP> objects) {
        super(ctx, R.layout.main_content, objects);

        this.context = ctx;
        this.data = objects;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(
            final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.main_content, null);

            ViewHolder holder = new ViewHolder();
            holder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
            holder.txtPublishDate = (TextView) convertView.findViewById(R.id.txtPublishDate);
            holder.txtSource = (TextView) convertView.findViewById(R.id.txtSource);
            holder.txtDesc = (TextView) convertView.findViewById(R.id.txtDesc);
            holder.imgThumbnail = (ImageView) convertView.findViewById(R.id.imgThumbnail);

            convertView.setTag(holder);
        }

        MotoGP obj = data.get(position);
        int imgStart = obj.getDescription().indexOf("<");
        int imgEnd = obj.getDescription().indexOf("/>");
        String desc = obj.getDescription();
        while (imgStart > -1 && imgEnd > -1) {
            String strToBeReplaced = desc.substring(imgStart, imgEnd + 2);
            desc = desc.replaceAll(strToBeReplaced, "");
            imgStart = desc.indexOf("<");
            if (imgStart > -1) {
                imgEnd = desc.substring(imgStart).indexOf("/>") + imgStart;
            }
        }

        String date = CommonUtil.getDateFormat().format(obj.getPublishDate());

        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.txtTitle.setText(obj.getTitle());
        holder.txtSource.setText(obj.getSource());
        holder.txtPublishDate.setText(date);
        holder.txtDesc.setText(Html.fromHtml(desc));
        if (holder.task != null) {
            holder.task.cancel(true);
        }

        if (obj.isRead()) {
            holder.txtTitle.setTextColor(context.getResources().getColor(R.color.grey));
        } else {
            holder.txtTitle.setTextColor(context.getResources().getColor(R.color.black));
        }

        displayImage(obj, holder);

        return convertView;
    }

    /**
     * Display image in ImageView.
     *
     * @param motoGP the {@link MotoGP} that contains image
     * @param holder the ViewHolder object
     * */
    private void displayImage(
            final MotoGP motoGP,
            final ViewHolder holder) {
        // reset ImageView
        holder.imgThumbnail.setImageBitmap(null);

        if (motoGP.getThumbnail() == null) {
            return;
        }
        // display image
        AsyncTask<MotoGP, Integer, Bitmap> task = new AsyncTask<MotoGP, Integer, Bitmap>() {
            @Override
            protected Bitmap doInBackground(final MotoGP... params) {
                try {
                    BitmapCacheUtil cache = BitmapCacheUtil.getInstance();
                    Bitmap bmp = cache.getBitmapFromCache(motoGP.getThumbnail());
                    if (bmp == null) {
                        URL url = new URL(motoGP.getThumbnail());
                        InputStream stream = url.openConnection().getInputStream();
                        Bitmap bitmap = BitmapFactory.decodeStream(stream);
                        cache.addBitmapToCache(motoGP.getThumbnail(), bitmap);
                        return bitmap;
                    } else {
                        return bmp;
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(final Bitmap bitmap) {
                holder.imgThumbnail.setImageBitmap(bitmap);
            }
        };
        holder.task = task;

        try {
            int currentapiVersion = android.os.Build.VERSION.SDK_INT;
            if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                task.execute();
            }
        } catch (RejectedExecutionException e) {
            // do nothing
        }
    }

    /**
     * A helper class that contains widgets to display RSS Feed.
     * */
    static class ViewHolder {
        /**
         * TextView that holds title.
         * */
        private TextView txtTitle;
        /**
         * TextView that holds publish date.
         * */
        private TextView txtPublishDate;
        /**
         * TextView that holds news source.
         * */
        private TextView txtSource;
        /**
         * TextView that holds description.
         * */
        private TextView txtDesc;
        /**
         * ImageView that holds thumbnail.
         * */
        private ImageView imgThumbnail;
        /**
         * The task to load image.
         * */
        private AsyncTask<MotoGP, Integer, Bitmap> task;
    }

}
