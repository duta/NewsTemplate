package id.co.ptdmc.berita2.motogp;

import id.co.ptdmc.berita2.motogp.beans.MotoGP;
import id.co.ptdmc.berita2.motogp.services.AutoSportRSSReader;
import id.co.ptdmc.berita2.motogp.services.CrashDotNetRSSReader;
import id.co.ptdmc.berita2.motogp.services.DetikRSSReader;
import id.co.ptdmc.berita2.motogp.services.HighRevsRSSReader;
import id.co.ptdmc.berita2.motogp.services.MetroTVNewsRSSReader;
import id.co.ptdmc.berita2.motogp.services.OkeZoneRSSReader;
import id.co.ptdmc.berita2.motogp.services.OtomotifNetRSSReader;
import id.co.ptdmc.berita2.motogp.services.SindoNewsRSSReader;

import java.util.List;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * Main activity class.
 *
 * @author Rochmat Santoso
 * */
@SuppressWarnings("deprecation")
public final class MainActivity extends ActionBarActivity implements
        OnItemClickListener {

    /**
     * ProgressBar object.
     * */
    private ProgressBar progressLoad;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listData = (ListView) findViewById(R.id.listData);
        listData.setOnItemClickListener(this);

        progressLoad = (ProgressBar) findViewById(R.id.progressLoad);
        progressLoad.setVisibility(View.VISIBLE);

        // english
        //populateDataCrashDotNet();
        // indonesia
        populateDataOkeZone();

        // Advertisement
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
    }

    /**
     * Populate data of {@link ListView}.
     *
     * @param forceRepopulate force {@link ListView} to be re-populated
     * @param data a list of {@link MotoGP} to add
     * */
    private void populateData(
            final boolean forceRepopulate, final List<MotoGP> data) {
        if (data == null) {
            return;
        }
        ListView listData = (ListView) findViewById(R.id.listData);
        ListAdapter adapter = listData.getAdapter();
        if (adapter == null || forceRepopulate) {
            if (adapter == null) {
                adapter = new MainAdapter(this, data);
                listData.setAdapter(adapter);
                ((MainAdapter) adapter).setNotifyOnChange(true);
            } else {
                ((MainAdapter) adapter).clear();
                ((MainAdapter) adapter).addAll(data);
            }
        } else {
            ((MainAdapter) adapter).addAll(data);
            ((MainAdapter) adapter).notifyDataSetChanged();
        }
    }

    @Override
    public void onItemClick(
            final AdapterView<?> parent,
            final View view,
            final int position,
            final long id) {
        ListView listData = (ListView) findViewById(R.id.listData);
        ListAdapter adapter = listData.getAdapter();
        MotoGP obj = ((MainAdapter) adapter).getItem(position);
        obj.setRead(true);

        TextView txtTitle = (TextView) view.findViewById(R.id.txtTitle);
        txtTitle.setTextColor(getResources().getColor(R.color.grey));

        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("motoGP", obj);
        startActivity(intent);
    }

    /**
     * Get data from {@link CrashDotNetRSSReader}.
     * */
    private void populateDataCrashDotNet() {
        AsyncTask<String, Integer, List<MotoGP>> task = new AsyncTask<String, Integer, List<MotoGP>>() {
            @Override
            protected List<MotoGP> doInBackground(final String... params) {
                CrashDotNetRSSReader obj = new CrashDotNetRSSReader();
                List<MotoGP> list = obj.read();
                return list;
            }

            @Override
            protected void onPostExecute(final List<MotoGP> result) {
                populateData(true, result);
                populateDataAutoSport();
            }

            @Override
            protected void onCancelled() {
                progressLoad.setVisibility(View.GONE);
            }
        };
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    /**
     * Get data from {@link HighRevsRSSReader}.
     * */
    private void populateDataHighRevs() {
        AsyncTask<String, Integer, List<MotoGP>> task = new AsyncTask<String, Integer, List<MotoGP>>() {
            @Override
            protected List<MotoGP> doInBackground(final String... params) {
                HighRevsRSSReader obj = new HighRevsRSSReader();
                List<MotoGP> list = obj.read();
                return list;
            }

            @Override
            protected void onPostExecute(final List<MotoGP> result) {
                populateData(false, result);
                progressLoad.setVisibility(View.GONE);
            }

            @Override
            protected void onCancelled() {
                progressLoad.setVisibility(View.GONE);
            }
        };
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    /**
     * Get data from {@link AutoSportRSSReader}.
     * */
    private void populateDataAutoSport() {
        AsyncTask<String, Integer, List<MotoGP>> task = new AsyncTask<String, Integer, List<MotoGP>>() {
            @Override
            protected List<MotoGP> doInBackground(final String... params) {
                AutoSportRSSReader obj = new AutoSportRSSReader();
                List<MotoGP> list = obj.read();
                return list;
            }

            @Override
            protected void onPostExecute(final List<MotoGP> result) {
                populateData(false, result);
                populateDataHighRevs();
            }

            @Override
            protected void onCancelled() {
                progressLoad.setVisibility(View.GONE);
            }
        };
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    /**
     * Get data from {@link OkeZoneRSSReader}.
     * */
    private void populateDataOkeZone() {
        AsyncTask<String, Integer, List<MotoGP>> task = new AsyncTask<String, Integer, List<MotoGP>>() {
            @Override
            protected List<MotoGP> doInBackground(final String... params) {
                OkeZoneRSSReader obj = new OkeZoneRSSReader();
                List<MotoGP> list = obj.read();
                return list;
            }

            @Override
            protected void onPostExecute(final List<MotoGP> result) {
                populateData(true, result);
                populateDataMetroTV();
            }

            @Override
            protected void onCancelled() {
                progressLoad.setVisibility(View.GONE);
            }
        };
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    /**
     * Get data from {@link MetroTVNewsRSSReader}.
     * */
    private void populateDataMetroTV() {
        AsyncTask<String, Integer, List<MotoGP>> task = new AsyncTask<String, Integer, List<MotoGP>>() {
            @Override
            protected List<MotoGP> doInBackground(final String... params) {
                MetroTVNewsRSSReader obj = new MetroTVNewsRSSReader();
                List<MotoGP> list = obj.read();
                return list;
            }

            @Override
            protected void onPostExecute(final List<MotoGP> result) {
                populateData(false, result);
                populateDataOtomotifNet();
            }

            @Override
            protected void onCancelled() {
                progressLoad.setVisibility(View.GONE);
            }
        };
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    /**
     * Get data from {@link OtomotifNetRSSReader}.
     * */
    private void populateDataOtomotifNet() {
        AsyncTask<String, Integer, List<MotoGP>> task = new AsyncTask<String, Integer, List<MotoGP>>() {
            @Override
            protected List<MotoGP> doInBackground(final String... params) {
                OtomotifNetRSSReader obj = new OtomotifNetRSSReader();
                List<MotoGP> list = obj.read();
                return list;
            }

            @Override
            protected void onPostExecute(final List<MotoGP> result) {
                populateData(false, result);
                populateDataSindoNews();
            }

            @Override
            protected void onCancelled() {
                progressLoad.setVisibility(View.GONE);
            }
        };
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    /**
     * Get data from {@link SindoNewsRSSReader}.
     * */
    private void populateDataSindoNews() {
        AsyncTask<String, Integer, List<MotoGP>> task = new AsyncTask<String, Integer, List<MotoGP>>() {
            @Override
            protected List<MotoGP> doInBackground(final String... params) {
                SindoNewsRSSReader obj = new SindoNewsRSSReader();
                List<MotoGP> list = obj.read();
                return list;
            }

            @Override
            protected void onPostExecute(final List<MotoGP> result) {
                populateData(false, result);
                populateDataDetik();
            }

            @Override
            protected void onCancelled() {
                progressLoad.setVisibility(View.GONE);
            }
        };
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    /**
     * Get data from {@link DetikRSSReader}.
     * */
    private void populateDataDetik() {
        AsyncTask<String, Integer, List<MotoGP>> task = new AsyncTask<String, Integer, List<MotoGP>>() {
            @Override
            protected List<MotoGP> doInBackground(final String... params) {
                DetikRSSReader obj = new DetikRSSReader();
                List<MotoGP> list = obj.read();
                return list;
            }

            @Override
            protected void onPostExecute(final List<MotoGP> result) {
                populateData(false, result);
                progressLoad.setVisibility(View.GONE);
            }

            @Override
            protected void onCancelled() {
                progressLoad.setVisibility(View.GONE);
            }
        };
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            task.execute();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
            } else {
                upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(upIntent);
            }
            return true;
        } else if (id == R.id.action_klasemen) {
            Intent intent = new Intent(this, KlasemenActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

}
