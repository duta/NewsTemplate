package id.co.ptdmc.berita2.motogp.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

/**
 * General purpose utility class.
 *
 * @author Rochmat Santoso
 * */
public final class CommonUtil {

    /**
     * General date formatter instance.
     * */
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
            "dd MMM yyyy", Locale.US);

    /**
     * Private constructor.
     * */
    private CommonUtil() {
    }

    /**
     * General date formatter instance.
     *
     * @return date formatter
     * */
    public static SimpleDateFormat getDateFormat() {
        DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT"));
        return DATE_FORMAT;
    }

}
