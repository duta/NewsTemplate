/**
 * A package that contains utility classes.
 *
 * @author Rochmat Santoso
 */
package id.co.ptdmc.berita2.motogp.utils;
