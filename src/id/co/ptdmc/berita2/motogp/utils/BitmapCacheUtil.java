package id.co.ptdmc.berita2.motogp.utils;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

/**
 * A utility class that handles bitmap's cache.
 *
 * @author Rochmat Santoso
 * */
public final class BitmapCacheUtil {

    /**
     * Singleton instance of this class.
     * */
    private static final BitmapCacheUtil INSTANCE = new BitmapCacheUtil();

    /**
     * Use 1/8th of the available memory for this memory cache.
     * */
    private static final int MEMORY_PARTITION = 8;

    /**
     * Kilobytes.
     * */
    private static final int KILO_BYTES = 1024;

    /**
     * Memory cache.
     * */
    private LruCache<String, Bitmap> cache;

    /**
     * @return instance of this class
     * */
    public static BitmapCacheUtil getInstance() {
        return INSTANCE;
    }

    /**
     * Private constructor.
     * */
    private BitmapCacheUtil() {
        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / KILO_BYTES);
        int cacheSize = maxMemory / MEMORY_PARTITION;

        cache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(final String key, final Bitmap bitmap) {
                return bitmap.getByteCount() / KILO_BYTES;
            }
        };
    }

    /**
     * Add a bitmap to the cache.
     *
     * @param key bitmap's key
     * @param bitmap the bitmap to cache
     * */
    public void addBitmapToCache(final String key, final Bitmap bitmap) {
        if (getBitmapFromCache(key) == null) {
            cache.put(key, bitmap);
        }
    }

    /**
     * Get a bitmap from the cache.
     *
     * @param key the bitmap's key
     * @return the bitmap
     * */
    public Bitmap getBitmapFromCache(final String key) {
        return cache.get(key);
    }

}
