package id.co.ptdmc.berita2.motogp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

/**
 * Activity class that contains klasemen information.
 *
 * @author Rochmat Santoso
 * */
@SuppressWarnings("deprecation")
public final class KlasemenActivity extends ActionBarActivity {

    /**
     * Klasemen URL.
     * */
    private static final String URL = "http://www.crash.net/motogp/championship_tables/content.html";

    /**
     * ProgressBar object.
     * */
    private ProgressBar progressLoad;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_klasemen);

        progressLoad = (ProgressBar) findViewById(R.id.progressLoad);
        progressLoad.setVisibility(View.VISIBLE);

        final WebView webKlasemen = (WebView) findViewById(R.id.webKlasemen);
        webKlasemen.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(final WebView view, final String url) {
                super.onPageFinished(view, url);
                progressLoad.setVisibility(View.GONE);
            }
        });
        webKlasemen.loadUrl(URL);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.klasemen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
            } else {
                upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(upIntent);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
