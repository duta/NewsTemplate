package id.co.ptdmc.berita2.motogp;

import id.co.ptdmc.berita2.motogp.beans.MotoGP;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

/**
 * Detail Activity class that contains a {@link WebView}.
 *
 * @author Rochmat Santoso
 * */
@SuppressWarnings("deprecation")
public final class DetailActivity extends ActionBarActivity {

    /**
     * ProgressBar object.
     * */
    private ProgressBar progressLoad;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        progressLoad = (ProgressBar) findViewById(R.id.progressLoad);
        progressLoad.setVisibility(View.VISIBLE);

        Bundle bundle = getIntent().getExtras();
        MotoGP motoGP = (MotoGP) bundle.get("motoGP");

        WebView webDetail = (WebView) findViewById(R.id.webDetail);
        webDetail.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(final WebView view, final String url) {
                super.onPageFinished(view, url);
                progressLoad.setVisibility(View.GONE);
            }
        });
        webDetail.loadUrl(motoGP.getLink());
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                TaskStackBuilder.create(this).addNextIntentWithParentStack(upIntent).startActivities();
            } else {
                upIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(upIntent);
            }
            return true;
        } else if (id == R.id.action_klasemen) {
            Intent intent = new Intent(this, KlasemenActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

}
